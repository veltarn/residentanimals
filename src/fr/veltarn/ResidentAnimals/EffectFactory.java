package fr.veltarn.ResidentAnimals;

import org.bukkit.plugin.Plugin;

public class EffectFactory
{
  public static Effect getRandomEffect( Plugin plugin )
  {
    int pCentageGetExplode = 10;
    int pCentageGetNone = 90;

    int number = (int)(Math.random() * ( 100.0D - 0 ) + 1 ) + 0;
    Effect effect = null;
    
    if (number < 40) {
      effect = new ExplosionEffect(plugin);
    }
    else if (number < 70) {
      effect = new ArtificeEffect(plugin);
    } else if (number < 90) {
      effect = new SnowballEffect(plugin);
    } else if( number > 90 ) {
	effect = new LavaEffect( plugin );
    }
    
    
    
    
    
    
    
    /*if (number < 40)
    {
      effect = new ExplosionEffect(plugin);
    }
    else if (number < 70)
    {
      effect = new LavaEffect(plugin);
    } else if (number < 90)
    {
      effect = new SnowballEffect(plugin);
    } else if (number > 90) {
      effect = new Effect(plugin);
    }*/
    return effect;
  }
}