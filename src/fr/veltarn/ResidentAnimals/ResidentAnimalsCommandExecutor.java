package fr.veltarn.ResidentAnimals;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ResidentAnimalsCommandExecutor
  implements CommandExecutor
{
  private ResidentAnimals plugin;

  public ResidentAnimalsCommandExecutor( ResidentAnimals plugin )
  {
    this.plugin = plugin;
  }
  
  @Override
  public boolean onCommand( CommandSender sender, Command command, String label, String[] args )
  {
    if ( command.getName().equalsIgnoreCase( "clearMaledictions" ) ) {
      this.plugin.clearMaledictions();
      sender.sendMessage( "Toutes les malédictions des joueurs ont été supprimées!" );
    }
    return true;
  }
}