package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;

public class EffectEntity
{
  private RemoteEntity m_remoteEntity;
  private Effect m_effect;

  public EffectEntity( RemoteEntity remoteEntity, Effect effect )
  {
    this.m_remoteEntity = remoteEntity;
    this.m_effect = effect;
  }

  public RemoteEntity getRemoteEntity() {
    return this.m_remoteEntity;
  }

  public Effect getEffect() {
    return this.m_effect;
  }

  public void doEffect() {
    getEffect().doEffect();
  }

  @Override()
  public String toString()
  {
    return this.m_remoteEntity.getBukkitEntity().toString() + " with effect " + this.m_effect.toString();
  }
}