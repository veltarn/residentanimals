package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import java.util.TimerTask;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class LavaEffect extends Effect
{
  private int m_timerId = -1;
  private int delayBeforeFlow = 0;

  public LavaEffect(Plugin plugin) {
    super( plugin );
    this.delayBeforeFlow = (int)( Math.random() * ( ( 15000 - 5000 ) + 1) ) + 5000;
    TimerTask task = new TimerTask()
    {
      @Override
      public void run() {
        doEffect();
      }
    };
    this.m_timerId = this.m_plugin.getServer().getScheduler().scheduleSyncDelayedTask( this.m_plugin, task, this.delayBeforeFlow / this.serverTick );
  }

  @Override
  public void stopEffect()
  {
    this.m_plugin.getServer().getScheduler().cancelTask( this.m_timerId );
  }

  @Override
  public void doEffect()
  {
    Block b = this.effectEntity.getBukkitEntity().getWorld().getBlockAt( this.effectEntity.getBukkitEntity().getLocation() );
    b.setTypeId(10);
  }
}