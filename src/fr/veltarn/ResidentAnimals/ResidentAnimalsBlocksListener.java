/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.veltarn.ResidentAnimals;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class ResidentAnimalsBlocksListener implements Listener {
    private ResidentAnimals plugin = null;
    
    public ResidentAnimalsBlocksListener( ResidentAnimals plugin ) {
        this.plugin = plugin;
    }

    
    @EventHandler(priority=EventPriority.LOW)
    public void onExplosion( EntityExplodeEvent event ) {
        for( Block block : event.blockList() ) {
            block.setType( Material.AIR );
        }
    }
}
