package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import java.util.Random;
import java.util.TimerTask;
import org.bukkit.Color;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

/**
 * Sorry for the english, this is the FireworkEffect class
 * but due to incompatibilities with the org.bukkit.FireworkEffect class
 * I have been forced to rename that one...
 * @author Dante
 */
public class ArtificeEffect extends Effect {
    private int m_delayBeforeExplode;
    private FireworkEffect m_fireworkEffect;
    private int m_timerId = -1;
    
    public ArtificeEffect( Plugin plugin ) {
	super( plugin );
	
	Random rnd = new Random();
	
	m_delayBeforeExplode = rnd.nextInt( ( 4000 - 1500 ) + 1 ) + 1500;
	
	m_fireworkEffect = createFirework();
	
	TimerTask task = new TimerTask() {

	    @Override
	    public void run() {
		doEffect();
	    }
	};
	
	m_timerId = m_plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, task, m_delayBeforeExplode / this.serverTick, m_delayBeforeExplode / this.serverTick );
    }
    
    @Override
    public void stopEffect() {
	m_plugin.getServer().getScheduler().cancelTask( m_timerId );
    }
    
    @Override
    public void doEffect() {
	LivingEntity entity = getEntity().getBukkitEntity();
	Firework firework = entity.getWorld().spawn( entity.getLocation(), Firework.class );
	
	FireworkMeta data = (FireworkMeta)firework.getFireworkMeta();
	
	data.addEffect( m_fireworkEffect );
	data.setPower( 2 );
	
	firework.setFireworkMeta( data );
    }
    
    private FireworkEffect createFirework() {
	Builder fireworkBuilder = FireworkEffect.builder();
	
	fireworkBuilder.flicker( true );
	
	Color rndColor = getRandomColor();
	
	int flickerNumber = (int)( Math.random() * 10 );
	boolean flicker = false;
	
	if(  flickerNumber < 50 ) {
	    flicker = false;
	} else {
	    flicker = true;
	}
	
	int fireworkType = (int)( Math.random() * 100 );
	
	if( fireworkType <= 20 ) {
	    fireworkBuilder.with( Type.BALL );
	} else if( fireworkType <= 40 ) {
	    fireworkBuilder.with( Type.BALL_LARGE );
	} else if( fireworkType <= 60 ) {
	    fireworkBuilder.with( Type.STAR );
	} else if( fireworkType <= 80 ) {
	    fireworkBuilder.with( Type.BURST );
	} else if( fireworkType <= 100 ) {
	    fireworkBuilder.with( Type.CREEPER );
	}
	
	fireworkBuilder.withColor( rndColor );
	fireworkBuilder.flicker( flicker );
	
	return fireworkBuilder.build();
    }
    
    private Color getRandomColor() {
	Color color = Color.fromRGB( 0, 0, 0);
	
	Random rnd = new Random();
	
	int red	    = rnd.nextInt( ( 255 - 55 ) + 1) + 55;
	int blue    = rnd.nextInt( ( 255 - 55 ) + 1) + 55;
	int green   = rnd.nextInt( ( 255 - 55 ) + 1) + 55;
	
	color = color.setRed( red ).setBlue( blue ).setGreen( green );
	return color;
    }
}
