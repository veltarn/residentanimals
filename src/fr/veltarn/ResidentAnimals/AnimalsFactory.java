package fr.veltarn.ResidentAnimals;

import java.util.ArrayList;

import de.kumpelblase2.remoteentities.EntityManager;
import de.kumpelblase2.remoteentities.api.RemoteEntity;
import de.kumpelblase2.remoteentities.api.RemoteEntityType;
import de.kumpelblase2.remoteentities.entities.RemotePig;
import de.kumpelblase2.remoteentities.entities.RemoteCow;
import de.kumpelblase2.remoteentities.entities.RemoteSheep;
import de.kumpelblase2.remoteentities.entities.RemoteChicken;
import de.kumpelblase2.remoteentities.entities.RemoteHorse;
import de.kumpelblase2.remoteentities.entities.RemoteWolf;
import de.kumpelblase2.remoteentities.entities.RemoteOcelote;
import org.bukkit.Location;


public class AnimalsFactory
{  
  public static RemoteEntity getAnimalById( EntityManager entityManager, Location animalLocation, int id )
  {
    RemoteEntity remote = null;

    switch (id)
    {
        case 1:
          remote = entityManager.createEntity( RemoteEntityType.Pig, animalLocation, false );
          break;
        case 2:
          remote = entityManager.createEntity( RemoteEntityType.Cow, animalLocation, false );
          break;
        case 3:
          remote = entityManager.createEntity( RemoteEntityType.Sheep, animalLocation, false );
          break;
        case 4:
          remote = entityManager.createEntity( RemoteEntityType.Chicken, animalLocation, false );
          break;
        default:
          System.out.println( "[ERROR] Cannot determine animal id" );
          break;
    }

    return remote;
  }
  
  public static RemoteEntity getRandomAnimal( EntityManager entityManager, Location animalLocation )
  {
      ArrayList<RemoteEntityType> animalsList = new ArrayList<>();
      animalsList.add( RemoteEntityType.Pig );
      animalsList.add( RemoteEntityType.Cow );
      animalsList.add( RemoteEntityType.Sheep );
      animalsList.add( RemoteEntityType.Chicken );
      animalsList.add( RemoteEntityType.Horse );
      animalsList.add( RemoteEntityType.Wolf );
      animalsList.add( RemoteEntityType.Ocelot );
      
      int randomAnimalSelector = (int)( Math.random() * ( ( ( 7 - 1 ) ) + 1 ) );
      
      RemoteEntityType entityType = animalsList.get( randomAnimalSelector );
      RemoteEntity entity = null;
      
      /*switch( randomAnimalSelector ) 
      {
          case 1 : /* Pig */
             /* entity = new RemotePig
      }*/
      
      entity = entityManager.createEntity( entityType, animalLocation, false );
      
      return entity;
  }
  
  public static String getAnimalName( int id ) {
    String str = "";
    switch ( id )
    {
        case 1:
          str = "cochon";
          break;
        case 2:
          str = "vache";
          break;
        case 3:
          str = "mouton";
          break;
        case 4:
          str = "poulet";
          break;
    }

    return str;
  }

  public static int getAnimalId( String bukkitName ) {
    int id = 0;
    if ( bukkitName.equals( "CraftPig") )
    {
      id = 1;
    }
    else if ( bukkitName.equals( "CraftCow" ) )
    {
      id = 2;
    }
    else if ( bukkitName.equals( "CraftSheep") )
    {
      id = 3;
    }
    else if ( bukkitName.equals( "CraftChicken" ) )
    {
      id = 4;
    }

    return id;
  }
}