package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import java.util.List;
import java.util.TimerTask;
import org.bukkit.Server;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Snowball;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class SnowballEffect extends Effect
{
  private int m_timerId = -1;

  public SnowballEffect( Plugin plugin ) {
    super( plugin );

    TimerTask task = new TimerTask()
    {
      @Override
      public void run() {
        doEffect();
      }
    };
    this.m_timerId = this.m_plugin.getServer().getScheduler().scheduleSyncRepeatingTask( this.m_plugin, task, 1500 / this.serverTick, 1500 / this.serverTick );
  }

  @Override
  public void stopEffect()
  {
    this.m_plugin.getServer().getScheduler().cancelTask(this.m_timerId);
  }

  @Override
  public void doEffect()
  {
    Creature entity = (Creature)this.effectEntity.getBukkitEntity();
    List<Entity> entitiesList = entity.getNearbyEntities( 7, 7, 7 );
    int number = (int)(Math.random() * ( ( entitiesList.size() - 1 ) + 0 ) ) + 1 ;
    
    if( entitiesList.size() > 0 && number < entitiesList.size() )
    {
        Entity bEntity = entitiesList.get( number );
        if ( bEntity instanceof LivingEntity )
        {
          LivingEntity tEntity = (LivingEntity)bEntity;
          entity.setTarget( tEntity );
          entity.launchProjectile( Snowball.class );
        }
    }
  }
}