package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class ResidentAnimalsListener implements Listener
{
  private ResidentAnimals plugin;

  public ResidentAnimalsListener(ResidentAnimals plugin)
  {
    this.plugin = plugin;
  }

  @EventHandler(priority=EventPriority.LOW)
  public void onPlayerInteractEntity(PlayerInteractEntityEvent event)
  {
    Player p = event.getPlayer();
    /*if ((event.getRightClicked() instanceof Animals))
    {
      RemoteEntity re = this.plugin.getEntityManager().createEntity(RemoteEntityType.Sheep, event.getRightClicked().getLocation(), false);
      re.getMind().clearBehaviours();
      re.getMind().clearMovementDesires();
      re.getMind().clearTargetingDesires();
      re.getMind().addMovementDesire(new DesireFollowSpecific(event.getPlayer(), 5.0F, 100.0F), 1);

      re.setSpeed(1.1F);

      System.out.println(re.isStationary());
    }*/
    if( p.getInventory().getItemInHand().getTypeId() == 272 )
    {
        if ( event.getRightClicked() instanceof LivingEntity )
        {
          LivingEntity targetedCreature  = (LivingEntity)event.getRightClicked();

          if ( this.plugin.hasMalediction( targetedCreature.getEntityId() ) )
          {
            if ( this.plugin.getMalediction( targetedCreature.getEntityId() ).getMaledictionCaster().getEntityId() == p.getEntityId() )
            {
              this.plugin.removeMalediction( targetedCreature.getEntityId() );
              if( targetedCreature instanceof Player )
              {
                Player target = (Player)targetedCreature;
                p.sendMessage( target.getDisplayName() + " n'est plus sous l'emprise de votre malédiction" );
              } else {
                  p.sendMessage( "Votre cible n'est plus affectée par la malédiction" );
              }
            }
          }
          else
          {
            //int number = (int)( Math.random() * ( ( 4 - 1 ) + 1 ) ) + 1;
            Malediction malediction = new Malediction( targetedCreature, p, this.plugin.getEntityManager()/*, number*/ );
            this.plugin.addMalediction( targetedCreature.getEntityId(), malediction );
            
            if( targetedCreature instanceof Player ) {
                Player target = (Player)targetedCreature;
                p.sendMessage( target.getDisplayName() + " est désormais sous l'emprise d'une malédiction!" );
            } else {
                p.sendMessage( "Cette pauvre créature est sous l'emprise de votre malédiction" );
            }
          }
        }
   }
  }

  /*@EventHandler(priority=EventPriority.LOW)
  public void onTargetEntity( EntityTargetLivingEntityEvent event )
  {
    System.out.println( "Targeting" );
    System.out.println( event.getTarget() );
  }*/

  @EventHandler(priority=EventPriority.LOW)
  public void onEntityDeath( EntityDeathEvent event ) {
      
    RemoteEntity entity = this.plugin.getEntityManager().getRemoteEntityFromEntity( event.getEntity() );
    if( entity != null )
        this.plugin.removeAnimal( entity );
    
    if ( event.getEntity() instanceof LivingEntity ) {
      LivingEntity creature = (LivingEntity)event.getEntity();
      
      if ( this.plugin.hasMalediction( creature.getEntityId() ) ) {
        if( creature instanceof Player )
        {
            Player player = (Player)creature;
            int chanceToRemoveMalediction = (int)( Math.random() * ( ( 100 - 1 ) + 1 ) ) + 1;
            if (chanceToRemoveMalediction > 80)
            {
              Player caster = this.plugin.getMalediction( creature.getEntityId() ).getMaledictionCaster();
              this.plugin.removeMalediction(player.getEntityId());
              player.sendMessage("Votre mort a eu pour effet de rompre la malédiction... chanceux!");
              caster.sendMessage( "La mort a été clémente avec " + player.getDisplayName() + " et l'a lavé de sa malédiction..." );
            }
        } else {
            this.plugin.getMalediction( creature.getEntityId() ).getMaledictionCaster().sendMessage( "Une créature que vous aviez maudite à trouvé la paix éternelle...");
            this.plugin.removeMalediction( creature.getEntityId() );
        }

      }

    }

  }
  
}