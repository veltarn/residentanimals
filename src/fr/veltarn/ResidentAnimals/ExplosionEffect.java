package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import java.util.Random;
import java.util.TimerTask;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.Plugin;

public class ExplosionEffect extends Effect {

    private int delayBeforeExplode = 0;

    private int m_timerId = -1;

    public ExplosionEffect(Plugin plugin) {
	super(plugin);

	this.delayBeforeExplode = (int) (Math.random() * ((7500 - 5000) + 1)) + 5000;

	TimerTask task = new TimerTask() {
	    @Override
	    public void run() {
		doEffect();
	    }
	};
	this.m_plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.m_plugin, task, this.delayBeforeExplode / this.serverTick);
    }

    @Override
    public void stopEffect() {
	this.m_plugin.getServer().getScheduler().cancelTask(this.m_timerId);
    }

    @Override
    public void doEffect() {
	LivingEntity entity = getEntity().getBukkitEntity();
	if (entity != null) {
	    World world = entity.getWorld();
	    Location entityLocation = entity.getLocation();
	    int chancesGenerateChicken = (int) (Math.random() * ((10 - 0) + 1)) + 1;

	    world.createExplosion(entityLocation, 5.0F, true);

	    if (chancesGenerateChicken >= 4) {

	    Random rnd = new Random();
	    int nbChickens = rnd.nextInt( ( 10 - 2 ) + 1 ) + 2;
	    for (int i = 0; i < nbChickens; i++) {
		    world.spawnEntity(entityLocation, EntityType.CHICKEN);
		}
	    }
	}
    }
}
