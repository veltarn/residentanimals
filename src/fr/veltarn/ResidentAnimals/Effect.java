package fr.veltarn.ResidentAnimals;

import de.kumpelblase2.remoteentities.api.RemoteEntity;
import org.bukkit.plugin.Plugin;

public class Effect
{
  protected RemoteEntity effectEntity;
  protected boolean playEffectOnDeath;
  protected Plugin m_plugin = null;
  protected int serverTick = 50;

  public Effect( Plugin plugin ) {
    this.m_plugin = plugin;

    int number = (int)( Math.random() * ( ( 100 - 1 ) + 1 ) ) + 1;
    if (number < 50)
      this.playEffectOnDeath = true;
    else
      this.playEffectOnDeath = false;
  }

  public void setRemoteEntity(RemoteEntity entity) {
    this.effectEntity = entity;
  }

  public RemoteEntity getEntity() {
    return this.effectEntity;
  }

  public boolean getPlayEffectOnDeath() {
    return this.playEffectOnDeath;
  }

  public void stopEffect()
  {
  }

  public void doEffect()
  {
  }
}