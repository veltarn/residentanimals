package fr.veltarn.ResidentAnimals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.TimerTask;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.InvalidConfigurationException;

import de.kumpelblase2.remoteentities.EntityManager;
import de.kumpelblase2.remoteentities.api.RemoteEntity;
import de.kumpelblase2.remoteentities.api.thinking.goals.DesireFollowSpecific;

import fr.veltarn.ResidentAnimals.Exceptions.NullRemoteEntityException;

public class Malediction {

    private LivingEntity m_targetedEntity = null;
    Player m_maledictionCaster = null;
    private EntityManager m_entityManager = null;

    private int animalCreationDelay = 1000;
    private final ArrayList<EffectEntity> m_entitiesList;
    private int maxEntities = 25;
    private int maxEntitiesToRandomGeneration = 12;
    private int m_taskId = 0;

    public Malediction(LivingEntity targetedEntity, Player maledictionCaster, EntityManager em/*, int animalId*/) {
	this.m_targetedEntity = targetedEntity;
	this.m_maledictionCaster = maledictionCaster;
	this.m_entityManager = em;

	ResidentAnimals plugin = (ResidentAnimals) em.getPlugin();
	this.animalCreationDelay = plugin.getAnimalGenerationDelay();
	this.maxEntitiesToRandomGeneration = plugin.getMaxEntitiesToGenerateAtOnce();
	//this.m_animalId = animalId;
	//In milliseconds
	this.m_entitiesList = new ArrayList<>();
	final Malediction that = this;

	TimerTask task = new TimerTask() {
	    @Override
	    public void run() {
		that.addEntity();
	    }
	};

	this.m_taskId = this.m_entityManager.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(plugin, task, 0, this.animalCreationDelay / 50);
    }

    public void addEntity() {
	if (this.m_entitiesList.size() < this.maxEntities) {
	    int chanceSpawnMultipleAnimals = (int) (Math.random() * ((100 - 0) + 1));
	    //System.out.println( "ChanceSpawnMultipleAnimals: " + chanceSpawnMultipleAnimals );
	    if (chanceSpawnMultipleAnimals <= 30) { /* Spawn multiple animals */

		int maxEntitiesToGenerate = this.maxEntitiesToRandomGeneration;
		if (this.m_entitiesList.size() > (this.maxEntities - this.maxEntitiesToRandomGeneration)) {
		    maxEntitiesToGenerate = this.maxEntities - this.maxEntitiesToRandomGeneration;
		}

		//Animals generation
		for (int i = 0; i < maxEntitiesToGenerate; ++i) {
		    try {
			EffectEntity effectEntity = createRandomEntity();
			m_entitiesList.add(effectEntity);
		    } catch (NullRemoteEntityException e) {
			System.out.println(e.getMessage());
			System.out.println("=======STACK TRACE=======");
			System.out.println(e.getStackTrace().toString());
			System.out.println("=======END STACK TRACE=======");
		    }
		}

	    } else {
		try {
		    EffectEntity effectEntity = createRandomEntity();
		    m_entitiesList.add( effectEntity );
		} catch (NullRemoteEntityException e) {
		    System.out.println(e.getMessage());
		    System.out.println("=======STACK TRACE=======");
		    System.out.println(e.getStackTrace().toString());
		    System.out.println("=======END STACK TRACE=======");
		}
	    }
	}
    }

    private EffectEntity createRandomEntity() throws NullRemoteEntityException {
	Location animalLocation = this.m_targetedEntity.getLocation();
	RemoteEntity randomEntity = AnimalsFactory.getRandomAnimal(this.m_entityManager, animalLocation);

	if (randomEntity != null) {
	    randomEntity.getBukkitEntity().setNoDamageTicks(30);
	    randomEntity.getMind().clearBehaviours();
	    randomEntity.getMind().clearMovementDesires();
	    randomEntity.getMind().clearTargetingDesires();
	    //@todo: Le remettre à 1 dès que possible, exception très fréquemment lancée quand un animal meures au MOMENT où l'effet est supposé être lancé
	    randomEntity.getMind().addMovementDesire(new DesireFollowSpecific(this.m_targetedEntity, 2, 100), 1);
	    randomEntity.setSpeed(0.9F);

	    Effect effect = EffectFactory.getRandomEffect(this.m_entityManager.getPlugin());
	    effect.setRemoteEntity(randomEntity);
	    EffectEntity effectEntity = new EffectEntity(randomEntity, effect);

	    return effectEntity;
	} else {
	    throw new NullRemoteEntityException("Cannot Create a Random Entity, remoteEntity is null");
	}
    }

    public boolean hasEntity(RemoteEntity entity) {
	for (EffectEntity cEntity : this.m_entitiesList) {
	    if (cEntity.getRemoteEntity() == entity) {
		return true;
	    }
	}
	return false;
    }

    public void removeEntity(RemoteEntity entity) {
	//List l = Collections.synchronizedList( this.m_entitiesList );
	EffectEntity toRemove = null;
	for (EffectEntity cEntity : this.m_entitiesList) {
	    if (cEntity.getRemoteEntity() == entity) {
		toRemove = cEntity;
	    }
	}

	if (toRemove != null) {
	    toRemove.getEffect().stopEffect();
	    if (toRemove.getEffect().getPlayEffectOnDeath()) {
		toRemove.doEffect();
	    }
	    this.m_entitiesList.remove(toRemove);
	}
    }

    public LivingEntity getTargetedEntity() {
	return this.m_targetedEntity;
    }

    public Player getMaledictionCaster() {
	return this.m_maledictionCaster;
    }

    public int getMaledictionAnimalId() {
	//return this.m_animalId;
	return 0;
    }

    public void stopMalediction() {
	this.m_entityManager.getPlugin().getServer().getScheduler().cancelTask(this.m_taskId);

	for (EffectEntity cEntity : this.m_entitiesList) {
	    cEntity.getEffect().stopEffect();
	    this.m_entityManager.removeEntity(cEntity.getRemoteEntity().getID());
	}
	this.m_entitiesList.clear();
    }
}
