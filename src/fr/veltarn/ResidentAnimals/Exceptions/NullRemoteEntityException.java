package fr.veltarn.ResidentAnimals.Exceptions;

import java.lang.Exception;
/**
 *
 * @author Dante
 */
public class NullRemoteEntityException extends Exception {
    public NullRemoteEntityException() {
	super();
    }
    
    public NullRemoteEntityException( String s ) {
	super( s );
    }
}
