package fr.veltarn.ResidentAnimals;

import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;

import de.kumpelblase2.remoteentities.EntityManager;
import de.kumpelblase2.remoteentities.RemoteEntities;
import de.kumpelblase2.remoteentities.api.RemoteEntity;
import org.bukkit.Server;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.InvalidConfigurationException;

public class ResidentAnimals extends JavaPlugin
{
  private ResidentAnimalsListener animalsListener = new ResidentAnimalsListener(this);
  private ResidentAnimalsBlocksListener blocksListener = new ResidentAnimalsBlocksListener( this );
  private ArrayList<Malediction> m_maledictionsList = new ArrayList<>();
  //private HashMap<String, Malediction> m_animalsPlayerTable = new HashMap<>();
  private ResidentAnimalsCommandExecutor executor;
  private PluginDescriptionFile pluginDescription;
  private EntityManager entityManager;
  
  private int animalGenerationDelay = 0;
  private int maxEntitiesToGenerateAtOnce = 0;
  
  @Override
  public void onEnable()
  {
    this.pluginDescription = getDescription();
    this.initConfigurationFile();
    this.executor = new ResidentAnimalsCommandExecutor( this );

    this.getCommand("clearMaledictions").setExecutor( this.executor );
    this.entityManager = RemoteEntities.createManager( this );
    
    PluginManager pm = getServer().getPluginManager();
    pm.registerEvents( this.animalsListener, this );
    pm.registerEvents( this.blocksListener, this );

    System.out.println( "[" + this.pluginDescription.getName() + " " + this.pluginDescription.getVersion() + "] Enabled" );
  }
  
  @Override
  public void onDisable()
  {
    System.out.println( "Clearing maledictions" );
    clearMaledictions();
    System.out.println( "[" + this.pluginDescription.getName() + " " + this.pluginDescription.getVersion() + "] Unloaded" );
  }
  
  private void initConfigurationFile() {
      File file = new File( "plugins/ResidentAnimals/maledictions.config.yml" );
      
      if( !file.exists() ) {
          YamlConfiguration config = new YamlConfiguration();
          
          try 
          {
            config.load( file );
          } catch (FileNotFoundException e ) {
              System.out.println( "File loading has raised an FileNotFoundException: " + e.getMessage() );
          } catch( IOException e ) {
              System.out.println( "File loading has raised an IOException: " + e.getMessage() );
          } catch( InvalidConfigurationException e ) {
              System.out.println( "File loading has raised an InvalidConfigurationException: " + e.getMessage() );
          }
          
          config.set( "AnimalGenerationDelay", 1000 );
          config.set( "maxEntitiesToGenerateAtOnce", 12 );
          this.animalGenerationDelay = 1000;
          this.maxEntitiesToGenerateAtOnce = 12;
          try {              
            config.save( file );
          } catch ( IOException e ) {
              System.out.println( "An exception has occured during file writing (IOException): " + e.getMessage() );
          }
      } else {          
          try {
            file.createNewFile();
          } catch( IOException e ) {
              System.out.println( "Creating new file has raised an exception (IOException): " + e.getMessage() );
          }
          YamlConfiguration config = new YamlConfiguration();
          
          try 
          {
            config.load( file );
          } catch (FileNotFoundException e ) {
              System.out.println( "File loading has raised an FileNotFoundException: " + e.getMessage() );
          } catch( IOException e ) {
              System.out.println( "File loading has raised an IOException: " + e.getMessage() );
          } catch( InvalidConfigurationException e ) {
              System.out.println( "File loading has raised an InvalidConfigurationException: " + e.getMessage() );
          }
          
          this.animalGenerationDelay = (int)config.get( "AnimalGenerationDelay" );
          this.maxEntitiesToGenerateAtOnce = (int)config.get( "maxEntitiesToGenerateAtOnce" );
      }
  }

  public int getAnimalGenerationDelay() {
      return this.animalGenerationDelay;
  }
  
  public int getMaxEntitiesToGenerateAtOnce() {
      return this.maxEntitiesToGenerateAtOnce;
  }
  
  public EntityManager getEntityManager() {
    return this.entityManager;
  }

  /*public HashMap<String, Malediction> getMaledictionTable() {
    return this.m_animalsPlayerTable;
  }*/
  public ArrayList<Malediction> getMaledictions() {
      return this.m_maledictionsList;
  }

  public boolean addMalediction( int entityId, Malediction malediction ) {
      boolean found = false;
      Malediction otherMalediction = null;
      for( Malediction cMalediction : this.m_maledictionsList ) {
          if( cMalediction.getTargetedEntity().getEntityId() == entityId ) {
              found = true;
              otherMalediction = cMalediction;
              break;
          }
      }
      
      if( found ) {
          //If we found that a malediction has already been casted on the player...
          //We proceed to another test
          if( otherMalediction != null )
          {
              //If the existing malediction haven't been casted by the actual caster
            if( otherMalediction.getMaledictionCaster().getEntityId() != malediction.getMaledictionCaster().getEntityId() ) {
                this.m_maledictionsList.add( malediction );
                return true;
            } else {
                return false;
            }
          } else {
              return false;
          }
      } else {
          this.m_maledictionsList.add( malediction );
          return true;
      }
  }
  /*public boolean addMalediction( String playerName, Malediction malediction )
  {
    if ( !this.m_animalsPlayerTable.containsKey( playerName ) )
    {
      this.m_animalsPlayerTable.put( playerName, malediction );
      return true;
    } else if( this.m_animalsPlayerTable.get( playerName ).getMaledictionCaster().getDisplayName().equals( malediction.getMaledictionCaster().getDisplayName() ) ) {
        
    }

    return false;
  }*/

  public Malediction getMalediction( int entityId )
  {
    for( Malediction cMalediction : this.m_maledictionsList ) 
    {
        if( cMalediction.getTargetedEntity().getEntityId() == entityId ) {
            return cMalediction;
        }
    }
    return null;
    //return this.m_animalsPlayerTable.get( playerName );
  }

  public void clearMaledictions() {
    for ( Malediction malediction : this.m_maledictionsList )
    {
      malediction.stopMalediction();
    }
    this.m_maledictionsList.clear();
  }

  public void removeMalediction( int entityId ) {
    boolean found = false;
    Malediction malediction = null;
    for( Malediction cMalediction : this.m_maledictionsList ) {
        if( cMalediction.getTargetedEntity().getEntityId() == entityId ) {
            found = true;
            malediction = cMalediction;
            break;
        }
    }
    
    if( found && malediction != null ) {
        malediction.stopMalediction();
        this.m_maledictionsList.remove( malediction );
    }
    /*this.m_animalsPlayerTable.get(key).stopMalediction();
    this.m_animalsPlayerTable.remove( key );*/
  }

  public boolean hasMalediction( int entityId ) {
      for( Malediction cMalediction : this.m_maledictionsList ) {
          if( cMalediction.getTargetedEntity().getEntityId() == entityId ) {
              return true;
          }
      }
    /*if ( this.m_animalsPlayerTable.containsKey( player ) )
    {
      return true;
    }*/

    return false;
  }

  public void removeAnimal( RemoteEntity animal )
  {
    boolean found = false;
    Malediction cMalediction = null;
    for ( Malediction malediction : this.m_maledictionsList )
    {
      if ( malediction.hasEntity( animal ) )
      {
          cMalediction = malediction;
         found = true;
      }
    }
    if( found ) {
        if( cMalediction != null )
            cMalediction.removeEntity( animal );
    }
  }  
}